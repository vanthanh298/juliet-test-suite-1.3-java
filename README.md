# Juliet Test Suite 1.3 phiên bản Java

1. Cài đặt [SonarQube](https://www.sonarqube.org/) về máy tính

2. Tạo project: Projects -> Add Project -> `Manually`

3. Nhập `Project key` và `Project Name` -> click vào `setUp`

4. Tạo token, nhập một key vào ô `Generate a token` -> Click `Generate` -> Click `Continue`

5. Tuỳ vào môi trường/loại source bạn muốn test để chọn. Ở đây mình chọn môi trường của mình là `Other (for JS, TS, Go, Python, PHP, ...)` -> `macOS`
Các bạn sẽ được 1 đoạn lệnh chạy ở màn hình console (Ví dụ của mình)
```command
sonar-scanner \
  -Dsonar.projectKey=xxxx:xxx \
  -Dsonar.sources=. \
  -Dsonar.host.url=http://localhost:9000 \
  -Dsonar.login=d91b97967704e64a3786d195d6234fddff002641\
  -Dsonar.java.binaries=target
```

> Lưu ý: với trường hợp có các file .java cần chỉ thị vị trí file build `-Dsonar.java.binaries=target`

6. Tải bộ source về dùng maven clean install

7. Sử dụng câu lệnh command để chạy cậu lệnh tại bước thứ 5. (cần cd vào thư mục chưa source)

# Juliet Test Suite phiên bản 1.3
Juliet Test Suite chi tiết [tại đây](https://samate.nist.gov/SRD/around.php#juliet_documents)

Phiên bản hiện tại chỉ thực hiện một số ví dụ, các bạn muốn thêm các CWE thì có thể copy các thư mục 
Từ `/src/testcases` của phiên bản Juliet Test Suite java vừa tải về vào thư mục `/src/main/java/testcases/` của project
Sau đó vào trong thư mục CWExxx_xxxx bạn vừa copy, xoá thư mục `antbuild`.
clean instal lại Project nhé



