package com;

import com.google.gson.Gson;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.bytecode.BadBytecode;
import javassist.bytecode.CodeIterator;

/**
 *
 * @author vanthanh
 */
public class ScanMethod {

    public static class MethodScan {

        private String dirName;
        private String fileName;
        private String packageName;
        private String name;
        private int startLine;
        private int endLine;

        public String getDirName() {
            return dirName;
        }

        public void setDirName(String dirName) {
            this.dirName = dirName;
        }

        public String getPackageName() {
            return packageName;
        }

        public void setPackageName(String packageName) {
            this.packageName = packageName;
        }

        public String getFileName() {
            return fileName;
        }

        public void setFileName(String fileName) {
            this.fileName = fileName;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getStartLine() {
            return startLine;
        }

        public void setStartLine(int startLine) {
            this.startLine = startLine;
        }

        public int getEndLine() {
            return endLine;
        }

        public void setEndLine(int endLine) {
            this.endLine = endLine;
        }

    }

    public static class ReportScan {

        private String dir;
        private String cls;
        private int line;
        private int badt;
        private int badg;
        private int goodt;
        private int goodg;
    }

    public static void main(String[] args) throws Exception {
        String path = System.getProperty("user.dir");
        String codePath = path + "/src/main/java/testcases";

        //Đọc các file trong class test
        Map<String, List<MethodScan>> classs = new HashMap<>();
        File directoryPath = new File(codePath);
        File filesList[] = directoryPath.listFiles();
        for (File file : filesList) {
            if (!file.getName().startsWith("CWE")) {
                continue;
            }

            if (classs.get(file.getName()) == null) {
                classs.put(file.getName(), new ArrayList<>());
            }

            File folder = new File(file.getAbsolutePath());
            File subFileList[] = folder.listFiles();
            for (File subFile : subFileList) {
                if (!subFile.getName().startsWith("CWE")) {
                    continue;
                }
                try {
                    String pkg = "testcases." + file.getName() + "." + (subFile.getName().replace(".java", ""));

                    ClassPool pool = ClassPool.getDefault();
                    CtClass cc = pool.get(pkg);
                    for (CtMethod method : cc.getDeclaredMethods()) {
                        if (!method.getName().startsWith("bad") && !method.getName().startsWith("good")) {
                            continue;
                        }
                        int startlineNumber = method.getMethodInfo().getLineNumber(0);
                        int endLineNumber = startlineNumber;
                        CodeIterator iter = method.getMethodInfo().getCodeAttribute().iterator();
                        while (iter.hasNext()) {
                            try {
                                int pos = iter.next();
                                endLineNumber = method.getMethodInfo().getLineNumber(pos);
                            } catch (BadBytecode e) {
                                throw new CannotCompileException(e);
                            }
                        }
                        MethodScan methodScan = new MethodScan();
                        methodScan.setName(method.getName());
                        methodScan.setDirName(file.getName());
                        methodScan.setFileName(subFile.getName());
                        methodScan.setPackageName(pkg);
                        methodScan.setStartLine(startlineNumber);
                        if (methodScan.dirName.startsWith("CWE209")) {
                            methodScan.setStartLine(startlineNumber - 4);
                        } else {
                            methodScan.setStartLine(startlineNumber - 2);
                        }
                        methodScan.setEndLine(endLineNumber);
                        classs.get(file.getName()).add(methodScan);
//                        System.out.println("Method name: " + method.getName() + " line " + startlineNumber + " -> " + endLineNumber);
                    }
                } catch (Exception e) {
                    System.out.println("Error: " + e.getMessage());
                    System.out.println("File name: " + subFile.getName());
                    System.out.println();
//                    break;
                }
            }
        }

        Map<String, Map<String, Integer>> data = new HashMap<>();
        for (Map.Entry<String, List<MethodScan>> entry : classs.entrySet()) {
            String cls = entry.getKey();
            List<ScanMethod.MethodScan> methods = entry.getValue();

            if (data.get(cls) == null) {
                data.put(cls, new HashMap<>());
            }
            for (MethodScan method : methods) {
                if (data.get(cls).get(method.getName()) == null) {
                    data.get(cls).put(method.getName(), 0);
                }
                data.get(cls).put(method.getName(), data.get(cls).get(method.getName()) + 1);
            }
        }
        Gson gson = new Gson();
        System.out.println(" ------ ");
        System.out.println(gson.toJson(data));
    }
}
