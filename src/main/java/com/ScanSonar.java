package com;

import com.google.gson.Gson;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.bytecode.BadBytecode;
import javassist.bytecode.CodeIterator;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author vanthanh
 */
public class ScanSonar {

    public static class MethodScan {

        private String dirName;
        private String fileName;
        private String packageName;
        private String name;
        private int startLine;
        private int endLine;

        public String getDirName() {
            return dirName;
        }

        public void setDirName(String dirName) {
            this.dirName = dirName;
        }

        public String getPackageName() {
            return packageName;
        }

        public void setPackageName(String packageName) {
            this.packageName = packageName;
        }

        public String getFileName() {
            return fileName;
        }

        public void setFileName(String fileName) {
            this.fileName = fileName;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getStartLine() {
            return startLine;
        }

        public void setStartLine(int startLine) {
            this.startLine = startLine;
        }

        public int getEndLine() {
            return endLine;
        }

        public void setEndLine(int endLine) {
            this.endLine = endLine;
        }

    }

    public static class ReportScan {

        private String dir;
        private String cls;
        private int line;
        private int badt;
        private int badg;
        private int goodt;
        private int goodg;
    }

    public static void main(String[] args) throws Exception {
        String path = System.getProperty("user.dir");
        String tmpPath = path + "/src/main/java/com/reportSonar.xlsx";
        String codePath = path + "/src/main/java/testcases";

        //Đọc các file trong class test
        Map<String, List<MethodScan>> classs = new HashMap<>();
        File directoryPath = new File(codePath);
        File filesList[] = directoryPath.listFiles();
        for (File file : filesList) {
            if (!file.getName().startsWith("CWE")) {
                continue;
            }

            if (classs.get(file.getName()) == null) {
                classs.put(file.getName(), new ArrayList<>());
            }

            File folder = new File(file.getAbsolutePath());
            File subFileList[] = folder.listFiles();
            for (File subFile : subFileList) {
                if (!subFile.getName().startsWith("CWE")) {
                    continue;
                }
                try {
                    String pkg = "testcases." + file.getName() + "." + (subFile.getName().replace(".java", ""));

                    ClassPool pool = ClassPool.getDefault();
                    CtClass cc = pool.get(pkg);
                    for (CtMethod method : cc.getDeclaredMethods()) {
                        if (!method.getName().startsWith("bad") && !method.getName().startsWith("good")) {
                            continue;
                        }
                        int startlineNumber = method.getMethodInfo().getLineNumber(0);
                        int endLineNumber = startlineNumber;
                        CodeIterator iter = method.getMethodInfo().getCodeAttribute().iterator();
                        while (iter.hasNext()) {
                            try {
                                int pos = iter.next();
                                endLineNumber = method.getMethodInfo().getLineNumber(pos);
                            } catch (BadBytecode e) {
                                throw new CannotCompileException(e);
                            }
                        }
                        MethodScan methodScan = new MethodScan();
                        methodScan.setName(method.getName());
                        methodScan.setDirName(file.getName());
                        methodScan.setFileName(subFile.getName());
                        methodScan.setPackageName(pkg);
                        methodScan.setStartLine(startlineNumber);
                        if (methodScan.dirName.startsWith("CWE209")) {
                            methodScan.setStartLine(startlineNumber - 4);
                        } else {
                            methodScan.setStartLine(startlineNumber - 2);
                        }
                        methodScan.setEndLine(endLineNumber);
                        classs.get(file.getName()).add(methodScan);
//                        System.out.println("Method name: " + method.getName() + " line " + startlineNumber + " -> " + endLineNumber);
                    }
                } catch (Exception e) {
                    System.out.println("Error: " + e.getMessage());
                    System.out.println("File name: " + subFile.getName());
                    System.out.println();
//                    break;
                }
            }
        }

        List<ReportScan> report = new ArrayList<>();
        //Đọc file excel
        InputStream file = new FileInputStream(new File(tmpPath));
        XSSFWorkbook workbook = new XSSFWorkbook(file);
        XSSFSheet sheet = workbook.getSheetAt(0);

        List<String> bads = new ArrayList<>();
        List<String> goods = new ArrayList<>();

        for (Row row : sheet) {
            Cell cell = row.getCell(2);
            if (cell == null) {
                continue;
            }
            String str = row.getCell(2).toString();
            if (str.contains("testcasesupport") || !str.contains("testcases")) {
                continue;
            }

            String[] info = str.split("\\/");
            String dir = info[info.length - 2];
            if (!dir.startsWith("CWE")) {
                dir = info[info.length - 3];
            }
            String cls = info[info.length - 1].split(":")[0]; //.replaceAll(".java", "").trim();
            int line = Integer.valueOf(info[info.length - 1].split(":")[1]);

            //Tìm thư mục CWE
            List<MethodScan> methods = classs.get(dir);
            if (methods == null) {
                System.out.println("---> Dir doesn't exist: " + dir);
                continue;
            }
            ReportScan reportScan = new ReportScan();
            reportScan.dir = dir;
            reportScan.cls = cls;
            reportScan.line = line;

            for (MethodScan method : methods) {
                //Tìm class
                if (!method.getFileName().equals(cls)) {
                    continue;
                }
                if (!bads.contains(reportScan.cls + method.getName()) && method.getName().startsWith("bad") && line >= method.getStartLine() && line <= method.getEndLine()) {
//                    System.out.println("--> " + reportScan.cls + ":" + method.getName());
//                  bad thật : hàm bad + có lỗi
                    reportScan.badt += 1;
                    bads.add(reportScan.cls + method.getName());
                } else if (!goods.contains(reportScan.cls + method.getName()) && method.getName().startsWith("good") && line >= method.getStartLine() && line <= method.getEndLine()) {
                    //Bad giả: hàm good + có lỗi
//                    System.out.println("--> " + reportScan.cls + ":" + method.getName());
                    reportScan.badg += 1;
                    goods.add(reportScan.cls + method.getName());
                }
            }
            report.add(reportScan);
//            if (1 == 1) {
//                break;
//            }
        }

        //Good thật: hàm good + không lỗi
        for (Map.Entry<String, List<MethodScan>> entry : classs.entrySet()) {
            String key = entry.getKey();
            List<MethodScan> scans = entry.getValue();
            for (MethodScan method : scans) {
                if (!method.getName().startsWith("good")) {
                    continue;
                }
                System.out.println("- " + method.fileName + ":" + method.getName());
                ReportScan reportScan = new ReportScan();
                reportScan.dir = key;
                reportScan.cls = method.fileName;

                boolean gt = true;
                for (ReportScan rs : report) {
                    //Good thật: hàm good + không lỗi
                    if (method.getFileName().equals(rs.cls) && method.getName().startsWith("good") && rs.line >= method.getStartLine() && rs.line <= method.getEndLine()) {
                        gt = false;
                        break;
                    }
                }
                if (gt) {
                    reportScan.goodt += 1;
                    System.out.println("      -> goodt: " + method.getName());
                }
                System.out.println(" ");
                report.add(reportScan);
            }
        }

        //Good giả: hàm bad + không lỗi
        for (Map.Entry<String, List<MethodScan>> entry : classs.entrySet()) {
            String key = entry.getKey();
            List<MethodScan> scans = entry.getValue();
            for (MethodScan method : scans) {
                if (!method.getName().startsWith("bad")) {
                    continue;
                }
                System.out.println("- " + method.fileName + ":" + method.getName());
                ReportScan reportScan = new ReportScan();
                reportScan.dir = key;
                reportScan.cls = method.fileName;

                boolean gg = true;
                for (ReportScan rs : report) {
                    //Good thật: hàm good + không lỗi
                    //Good giả: hàm bad + không lỗi
                    if (method.getFileName().equals(rs.cls) && method.getName().startsWith("bad") && rs.line >= method.getStartLine() && rs.line <= method.getEndLine()) {
//                        System.out.println("--> " + method.fileName + ":" + method.getName());
                        gg = false;
                        break;
                    }
                }
                if (gg) {
                    reportScan.goodg += 1;
                    System.out.println("      -> goodg: " + method.getName());
                }

                System.out.println(" ");
                report.add(reportScan);
            }
        }

        Gson gson = new Gson();
//        System.out.println(gson.toJson(report));

        Map<String, ReportScan> groups = new HashMap<>();
        for (ReportScan reportScan : report) {
            if (groups.get(reportScan.dir) == null) {
                groups.put(reportScan.dir, new ReportScan());
            }
            groups.get(reportScan.dir).badg += reportScan.badg;
            groups.get(reportScan.dir).badt += reportScan.badt;
            groups.get(reportScan.dir).goodg += reportScan.goodg;
            groups.get(reportScan.dir).goodt += reportScan.goodt;
        }
        System.out.println(" ------ ");
        System.out.println(gson.toJson(groups));
    }
}
