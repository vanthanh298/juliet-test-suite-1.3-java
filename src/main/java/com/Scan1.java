package com;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.NotFoundException;
import javassist.bytecode.BadBytecode;
import javassist.bytecode.CodeIterator;

/**
 *
 * @author vanthanh
 */
public class Scan1 {

    public static class MethodScan {

        private String dirName;
        private String fileName;
        private String packageName;
        private String name;
        private int startLine;
        private int endLine;

        public String getDirName() {
            return dirName;
        }

        public void setDirName(String dirName) {
            this.dirName = dirName;
        }

        public String getPackageName() {
            return packageName;
        }

        public void setPackageName(String packageName) {
            this.packageName = packageName;
        }

        public String getFileName() {
            return fileName;
        }

        public void setFileName(String fileName) {
            this.fileName = fileName;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getStartLine() {
            return startLine;
        }

        public void setStartLine(int startLine) {
            this.startLine = startLine;
        }

        public int getEndLine() {
            return endLine;
        }

        public void setEndLine(int endLine) {
            this.endLine = endLine;
        }

    }

    public static void mainx(String[] args) throws Exception {
        ClassPool pool = ClassPool.getDefault();
        CtClass cc = pool.get("testcases.CWE193_Off_by_One_Error.CWE193_Off_by_One_Error__do_01");
        for (CtMethod method : cc.getDeclaredMethods()) {
            if (!method.getName().startsWith("bad") && !method.getName().startsWith("good")) {
                continue;
            }
            int startlineNumber = method.getMethodInfo().getLineNumber(0);
            int endLineNumber = startlineNumber;
            CodeIterator iter = method.getMethodInfo().getCodeAttribute().iterator();
            while (iter.hasNext()) {
                try {
                    int pos = iter.next();
                    endLineNumber = method.getMethodInfo().getLineNumber(pos);
                } catch (BadBytecode e) {
                    throw new CannotCompileException(e);
                }
            }
            System.out.println("Method name: " + method.getName() + " line " + startlineNumber + " -> " + endLineNumber);
        }
    }

    public static void main(String[] args) throws Exception {
        String path = System.getProperty("user.dir");
        String tmpPath = path + "/src/main/java/com/tmp.xlsx";
        String codePath = path + "/src/main/java/testcases";

        Map<String, List<MethodScan>> classs = new HashMap<>();

        File directoryPath = new File(codePath);
        File filesList[] = directoryPath.listFiles();
        for (File file : filesList) {
            if (!file.getName().startsWith("CWE")) {
                continue;
            }

            if (classs.get(file.getName()) == null) {
                classs.put(file.getName(), new ArrayList<>());
            }

            File folder = new File(file.getAbsolutePath());
            File subFileList[] = folder.listFiles();
            for (File subFile : subFileList) {
                if (!subFile.getName().startsWith("CWE")) {
                    continue;
                }
                try {
                    MethodScan methodScan = new MethodScan();
                    methodScan.setDirName(file.getName());
                    methodScan.setFileName(subFile.getName());
                    methodScan.setPackageName("testcases." + methodScan.getDirName() + "." + (methodScan.getFileName().replace(".java", "")));

                    ClassPool pool = ClassPool.getDefault();
                    CtClass cc = pool.get(methodScan.getPackageName());
                    for (CtMethod method : cc.getDeclaredMethods()) {
                        if (!method.getName().startsWith("bad") && !method.getName().startsWith("good")) {
                            continue;
                        }
                        int startlineNumber = method.getMethodInfo().getLineNumber(0);
                        int endLineNumber = startlineNumber;
                        CodeIterator iter = method.getMethodInfo().getCodeAttribute().iterator();
                        while (iter.hasNext()) {
                            try {
                                int pos = iter.next();
                                endLineNumber = method.getMethodInfo().getLineNumber(pos);
                            } catch (BadBytecode e) {
                                throw new CannotCompileException(e);
                            }
                        }
                        System.out.println("Method name: " + method.getName() + " line " + startlineNumber + " -> " + endLineNumber);
                    }

                    classs.get(file.getName()).add(methodScan);
                } catch (Exception e) {
                    System.out.println("Error: " + e.getMessage());
                    System.out.println("File name: " + subFile.getName());
                    System.out.println();
                    break;
                }
            }
            break;
        }
//        InputStream file = new FileInputStream(new File(tmpPath));
//        XSSFWorkbook workbook = new XSSFWorkbook(file);
//        XSSFSheet sheet = workbook.getSheetAt(0);
//        for (Row row : sheet) {
//            System.out.println("-> " + row.getRowNum());
//        }
    }
}
